from django.shortcuts import render

def index(request):
    return render(request, "index.html")

def filtered(request):
    return render(request, "filtered.html")

