from django.urls import path
from django.contrib.auth.decorators import login_required
from .views import index,filtered

app_name = 'core'

urlpatterns = [
    path('', index, name='index'),
    path('filtered/', filtered, name='filtered'),
]